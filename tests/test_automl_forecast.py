#!/usr/bin/env python

"""Tests for `automl_forecast` package."""

import pytest

import pandas as pd

from sklearn.linear_model import (
    HuberRegressor, LinearRegression, Lasso, Lars, LassoLars, Ridge,
    SGDRegressor, ElasticNet, BayesianRidge, TweedieRegressor
)
from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
import xgboost as xgb

from automl_forecast import (
    DEFAULT, RANDOM_STATE
)
from automl_forecast.automl_forecast import AutoML
from automl_forecast.data import Data
from automl_forecast.features import RelGrowth, Ratio, SimpleLags, AbsDelta, DeltaGrowth

from automl_forecast.objectives import (
    objective_hr, objective_en, objective_rf, objective_rr,
    objective_ls, objective_la, objective_ll, objective_br,
    objective_sg, objective_xb, objective_tr, objective_sr
)


# TODO config
def _test_automl_passives():
    is_error = False
    try:
        automl = AutoML(
            config={
                'methods': {
                    'lr': LinearRegression(),
                    'hr': HuberRegressor(max_iter=500),
                    'en': ElasticNet(random_state=RANDOM_STATE),
                    #'rr': Ridge(random_state=RANDOM_STATE),
                    #'rf': RandomForestRegressor(random_state=RANDOM_STATE),
                    'xb': xgb.XGBRegressor(
                        random_state=RANDOM_STATE,
                        booster='gblinear'
                    ),
                    'sg': SGDRegressor(
                        random_state=RANDOM_STATE,
                        loss='huber',
                    ),
                    # 'ls': Lasso(random_state=RANDOM_STATE),
                    # 'la': Lars(random_state=RANDOM_STATE),
                    'll': LassoLars(random_state=RANDOM_STATE),
                    'br': BayesianRidge(),
                    #'sr': SVR(kernel='linear')
                },
                'objective': {
                    'hr': objective_hr,
                    'en': objective_en,
                    #'rr': objective_rr,
                    'rf': objective_rf,
                    'xb': objective_xb,
                },
                'objective_n_trials': 30,
                'data': Data(
                    config={
                        'conn_str': 'mssql+pyodbc://MSSQL_DSN',
                        'db_name': 'Model_Passiv_FL_ML',
                        'base_cols': ['dept_id', 'report_date', 'grp', 'tb', 'gosb', ],
                        'groups': (1e+007, 5e+008,),
                        'to_sql_chunksize': 30,
                    }),
                'features': {
                    'clients_count': RelGrowth(), #config={'mean_month_qty': 3}
                    #'sdo_by_client_cur_acc': SimpleLags(),
                    #'balance_by_client_rur_crd': AbsDelta(),
                    DEFAULT:  Ratio(),
                },
                'metrics': ['sdo_by_client_rur_dep', 'clients_count', ],
                'auto_lag': 2,
                'backtest_months': 2,
                'forecast_months': 1,
                'lags_qty': 12,
                'is_calculate_backtest': True,
                'is_calculate_forecast': True,
                'is_load_backtest_to_db': True,
                'is_load_forecast_to_db': True,
            }
        )
        automl.calculate()

        quality = automl.get_quality()
        for metric in quality.keys():
            print(f'{metric}:')
            print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
    except BaseException as e:
        is_error = True
        print(e)
    assert is_error is False


def _test_automl_acquiring():
    is_error = False
    try:
        automl = AutoML(
            config={
                'data': Data(
                    config={
                        'conn_str': 'mssql+pyodbc://MSSQL_DSN',
                        'db_name': 'Model_Passiv_FL_ML',
                        'table_suffix': '_acquiring',
                        'base_cols': ['gosb', 'report_date', 'tb', 'grp', ],
                    }),
                'methods': {
                    'lr': LinearRegression(),
                    'hr': HuberRegressor(max_iter=500),
                    'en': ElasticNet(random_state=RANDOM_STATE),
                    'rr': Ridge(random_state=RANDOM_STATE),
                    'rf': RandomForestRegressor(random_state=RANDOM_STATE),
                    'xb': xgb.XGBRegressor(
                        random_state=RANDOM_STATE,
                        booster='gblinear',
                        n_estimators=3
                    ),
                    'sg': SGDRegressor(
                        random_state=RANDOM_STATE,
                        loss='huber',
                    ),
                    'ls': Lasso(random_state=RANDOM_STATE),
                    'la': Lars(random_state=RANDOM_STATE),
                    'br': BayesianRidge(),
                    'tr': TweedieRegressor(),
                    'sr': SVR(kernel='linear')
                },
                'objective': {
                    'hr': objective_hr,
                    'en': objective_en,
                    'rr': objective_rr,
                    'rf': objective_rf,
                    'xb': objective_xb,
                    'ls': objective_ls,
                    'la': objective_la,
                    'll': objective_ll,
                    'br': objective_br,
                    'tr': objective_tr,
                    'sr': objective_sr,
                },
                'objective_n_trials': 70,
                'df': pd.read_excel('data_acquiring.xlsx'),
                'gosb_column': 'gosb',
                'feature_prefix': 'lag',
                'is_calculate_backtest': True,
                'is_calculate_forecast': True,
                'is_load_backtest_to_db': True,
                'is_load_forecast_to_db': True,
            }
        )
        automl.calculate()

        quality = automl.get_quality()
        for metric in quality.keys():
            print(f'{metric}:')
            print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
    except BaseException as e:
        is_error = True
        print(e)
    assert is_error is False


def _test_automl_rko():
    is_error = False
    try:
        automl = AutoML(
            config={
                'data': Data(
                    config={
                        'server': '[HOST]',
                        'db_name': 'Model_ML_CIB',
                        'base_cols': ['gosb', 'report_date', 'tb', 'grp', ],
                }),
                'methods': {
                    'lr': LinearRegression(),
                    'hr': HuberRegressor(max_iter=5000),
                    'en': ElasticNet(random_state=RANDOM_STATE),
                    'rr': Ridge(random_state=RANDOM_STATE),
                    'rf': RandomForestRegressor(random_state=RANDOM_STATE),
                    'xb': xgb.XGBRegressor(
                        random_state=RANDOM_STATE,
                        booster='gblinear'),
                    'sg': SGDRegressor(
                        random_state=RANDOM_STATE,
                        loss='huber',
                    ),
                    'ls': Lasso(random_state=RANDOM_STATE),
                    'la': Lars(random_state=RANDOM_STATE),
                    'll': LassoLars(random_state=RANDOM_STATE),
                    'br': BayesianRidge(),
                    'tr': TweedieRegressor(),
                    'sr': SVR(kernel='linear')
                },
                '_objective': {
                    'hr': objective_hr,
                    'en': objective_en,
                    'rr': objective_rr,
                    'rf': objective_rf,
                    'xb': objective_xb,
                    'ls': objective_ls,
                    'la': objective_la,
                    'll': objective_ll,
                    'br': objective_br,
                    'tr': objective_tr,
                    'sr': objective_sr,
                },
                'objective_n_trials': 100,
                'df': pd.read_excel('df_rko.xlsx'),
                'gosb_column': 'gosb',
                'feature_prefix': 'x',
                'is_calculate_backtest': True,
                'is_calculate_forecast': False,
                'is_load_backtest_to_db': True,
                'is_load_forecast_to_db': False,
            }
        )
        automl.calculate()

        quality = automl.get_quality()
        for metric in quality.keys():
            print(f'{metric}:')
            print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
    except BaseException as e:
        is_error = True
        print(e)
    assert is_error is False


def _test_automl_rko_db():
    is_error = False
    try:
        automl = AutoML(
            config={
                'data': Data(
                    config={
                        'server': 'v-sap-12r2-002.ca.sbrf.ru',
                        'db_name': 'Model_ML_CIB',
                        'base_cols': ['gosb', 'report_date', 'tb', 'grp', ],
                        'groups': (1, 2, 3, 4, ),
                }),
                'methods': {
                    'lr': LinearRegression(),
                    'hr': HuberRegressor(max_iter=500),
                    'en': ElasticNet(random_state=RANDOM_STATE),
                    # 'rr': Ridge(random_state=RANDOM_STATE),
                    # 'rf': RandomForestRegressor(random_state=RANDOM_STATE),
                    'xb': xgb.XGBRegressor(
                        random_state=RANDOM_STATE,
                        booster='gblinear'),
                    'sg': SGDRegressor(
                        random_state=RANDOM_STATE,
                        loss='huber',
                    ),
                    # 'ls': Lasso(random_state=RANDOM_STATE),
                    # 'la': Lars(random_state=RANDOM_STATE),
                    'll': LassoLars(random_state=RANDOM_STATE),
                    'br': BayesianRidge(),
                    #'tr': TweedieRegressor(),
                    # 'sr': SVR(kernel='linear')
                },
                # 'objective': {
                #     'hr': objective_hr,
                #     'en': objective_en,
                #     # 'rr': objective_rr,
                #     # 'rf': objective_rf,
                #     'xb': objective_xb,
                #     # 'ls': objective_ls,
                #     # 'la': objective_la,
                #     'll': objective_ll,
                #     'br': objective_br,
                #     #'tr': objective_tr,
                #     # 'sr': objective_sr,
                # },
                # 'objective_n_trials': 100,
                'features': {
                    DEFAULT: RelGrowth(config={
                        'mean_month_qty': 1,
                        'is_add_sb': True}),
                    #DEFAULT: Ratio()
                },
                'is_features_depend_on_skip_lag': True,
                'auto_lag': 2,
                'backtest_months': 3,
                'lags_qty': 12,
                'lags_filter': [1, 12],
                'gosb_column': 'gosb',
                'is_calculate_backtest': True,
                'is_calculate_forecast': False,
                'is_load_backtest_to_db': True,
                'is_load_forecast_to_db': False,
            }
        )
        automl.calculate()

        frames = automl.get_data_frames()
        print (frames.keys())
        print (
            frames['backtest_turnover_lr_0']['X_train'],
            frames['backtest_turnover_lr_0']['df_result'],
        )

        quality = automl.get_quality()
        for metric in quality.keys():
            print(f'{metric}:')
            print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
    except BaseException as e:
        is_error = True
        print(e)
    assert is_error is False


def _test_automl_creditfl_db():
    is_error = False
    try:
        automl = AutoML(
            config={
                'data': Data(
                    config={
                        'server': 'v-sap-12r2-002.ca.sbrf.ru',
                        'db_name': 'Model_Credit_FL',
                        'base_cols': ['dept_id', 'report_date', 'grp', 'tb', 'gosb', ],
                        'groups': (1, ),
                }),
                'methods': {
                    'lr': LinearRegression(),
                    'hr': HuberRegressor(max_iter=5000),
                    'en': ElasticNet(random_state=RANDOM_STATE),
                    # 'rr': Ridge(random_state=RANDOM_STATE),
                    # 'rf': RandomForestRegressor(random_state=RANDOM_STATE),
                    'xb': xgb.XGBRegressor(
                        random_state=RANDOM_STATE,
                        booster='gblinear'),
                    'sg': SGDRegressor(
                        random_state=RANDOM_STATE,
                        loss='huber',
                    ),
                    # 'ls': Lasso(random_state=RANDOM_STATE),
                    # 'la': Lars(random_state=RANDOM_STATE),
                    #'ll': LassoLars(random_state=RANDOM_STATE),
                    #'br': BayesianRidge(),
                    #'tr': TweedieRegressor(),
                    # 'sr': SVR(kernel='linear')
                },
                'objective': {
                    'hr': objective_hr,
                    'en': objective_en,
                    # 'rr': objective_rr,
                    # 'rf': objective_rf,
                    'xb': objective_xb,
                    # 'ls': objective_ls,
                    # 'la': objective_la,
                    #'ll': objective_ll,
                    #'br': objective_br,
                    #'tr': objective_tr,
                    # 'sr': objective_sr,
                },
                'objective_n_trials': 50,
                'features': {
                    DEFAULT: Ratio(),
                },
                'metrics': ('331|0', ),
                'auto_lag': 6,
                'backtest_months': 6,
                'lags_qty': 16,
                'is_calculate_backtest': True,
                'is_calculate_forecast': False,
                'is_load_backtest_to_db': True,
                'is_load_forecast_to_db': False,
            }
        )
        automl.calculate()

        quality = automl.get_quality()
        for metric in quality.keys():
            print(f'{metric}:')
            print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
    except BaseException as e:
        is_error = True
        print(e)
    assert is_error is False


def test_cards_fl():
    try:
        automl = AutoML(
            config={
                'methods': {
                    'lr': LinearRegression(),
                    'hr': HuberRegressor(max_iter=5000),
                    'en': ElasticNet(random_state=RANDOM_STATE),
                    'xb': xgb.XGBRegressor(
                        random_state=RANDOM_STATE,
                        booster='gblinear'
                    ),
                    'sg': SGDRegressor(
                        random_state=RANDOM_STATE,
                        loss='huber',
                    ),
                    'll': LassoLars(random_state=RANDOM_STATE),
                    'br': BayesianRidge(),
                },
                'objective': {
                    'hr': objective_hr,
                    'xb': objective_xb,
                },
                'objective_n_trials': 100,
                'data': Data(
                    config={
                        'server': 'v-sap-12r2-002.ca.sbrf.ru',
                        'db_name': 'Model_card_FL',
                        'base_cols': ['dept_id', 'report_date', 'grp', 'tb', 'gosb', ],
                        'groups': (1, 3,),
                        'to_sql_chunksize': 50,
                        'table_suffix': '_relgrowth'
                    }),
                'features': {
                    #'новые выдачи': RelGrowth(),
                    DEFAULT: RelGrowth(),
                },
                'auto_lag': 6,
                'backtest_months': 6,
                'forecast_months': 2,
                'lags_qty': 12,
                'metrics': ['новые выдачи', ],
                'is_calculate_backtest': True,
                'is_calculate_forecast': True,
                'is_load_backtest_to_db': True,
                'is_load_forecast_to_db': False,
            }
        )
        automl.calculate()

        frames = automl.get_data_frames()
        print (frames.keys())
        print (frames['backtest_новые выдачи_hr_5']['df_result'].head())

        quality = automl.get_quality()
        for metric in quality.keys():
            print(f'{metric}:')
            print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
    except BaseException as e:
        print(e)
