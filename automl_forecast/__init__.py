"""Top-level package for AutoML Forecast."""

__author__ = """Eugena Mikhaylikova"""
__email__ = 'eugena.mihailikova@gmail.com'
__version__ = '0.1.0'

DEFAULT = 'default'

BACKTEST = 'backtest'
FORECAST = 'forecast'

LAYER_FACT = 'fact'
LAYER_FORECAST = 'forecast'

FACT_COL = 'fact'
GOSB_COL = 'dept_id'
GROUP_COL = 'grp'

RANDOM_STATE = 0
