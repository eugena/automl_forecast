from abc import ABC

import numpy as np

from automl_forecast import (
    FACT_COL, FORECAST
)


class Feature(ABC):
    """
    Abstract class for feature engineering
    """

    config = {}

    def __init__(self, config=None):
        """
        Initialization
        """
        self.config = self.config.copy()
        if config is not None and isinstance(config, dict):
            for key, value in config.items():
                self.config[key] = value

    def add(self, *args, **kwargs):
        """
        Adds a feature block
        """
        raise NotImplementedError("The 'add' method must be implemented")

    def recovery(self, *args, **kwargs):
        """
        Recovers values
        """
        raise NotImplementedError("The 'recovery' method must be implemented")


class SimpleLags(Feature):
    """
    Simple lags
    """
    prefix = 'lag'

    def add(self, *args, **kwargs):
        """
        Returns df without changes

        @return pd.DataFrame
        """
        return kwargs.get('df')

    def recovery(self, *args, **kwargs):
        """
        Recovers values

        @return pd.DataFrame
        """
        df_result = kwargs.get('df_result')
        method_name = kwargs.get('method_name')

        df_result['recovered'] = df_result['Y']
        df_result['method'] = method_name

        return df_result


class RelGrowth(Feature):
    """
    Relative growth
    """
    prefix = 'rel_growth'
    config = {
        'mean_month_qty': 1,
        'is_add_sb': True}

    def add(self, *args, **kwargs):
        """
        Adds relative growth block

        @return pd.DataFrame
        """
        df = kwargs.get('df')
        lag = kwargs.get('lag')
        lags_qty = kwargs.get('auto_ml').config.get('lags_qty')
        lags_filter = kwargs.get('auto_ml').config.get('lags_filter')
        is_features_depend_on_skip_lag = kwargs.get('auto_ml').config.get(
            'is_features_depend_on_skip_lag', False)

        df = self._add_block(df, FACT_COL, lag, lags_qty, lags_filter, self.prefix,
                             is_features_depend_on_skip_lag)
        if self.config.get('is_add_sb'):
            df = self._add_block(df, f'{FACT_COL}_sb', lag, lags_qty, lags_filter, f'{self.prefix}_sb',
                                 is_features_depend_on_skip_lag)

        return df

    def recovery(self, *args, **kwargs):
        """
        Recovers values

        @return pd.DataFrame
        """
        df_result = kwargs.get('df_result')
        df = kwargs.get('df')
        lag = kwargs.get('lag')
        method_name = kwargs.get('method_name')
        X_test = kwargs.get('X_test')

        df_result['Y'] **= (lag + 1)
        df_result['recovered'] = df.loc[
            df.index.isin(X_test.index),
            f'{FACT_COL}_1'] * df_result['Y']
        df_result['method'] = method_name

        return df_result

    def _add_block(self, df, column, lag, lags_qty, lags_filter, prefix, is_features_depend_on_skip_lag):
        """
        Adds a features block

        @return pd.DataFrame
        """
        df[f'{prefix}_0'] = (df[f'{column}_0'] /
                             df[f'{column}_1'].replace(0, 1e-4)) ** (1 / (lag + 1))

        lag_shift = lag if is_features_depend_on_skip_lag else 0

        for l in filter(
                lambda x: True if lags_filter is None else x in [0] + lags_filter,
                range(1, lags_qty - self.config.get('mean_month_qty') - lag_shift + 2)):

            df[f'{prefix}_{l}'] = df[f'{column}_{l}'] / \
                df[[f'{column}_{(l + 1) + i + lag_shift}' for i in range(
                    self.config.get('mean_month_qty'))]].mean(axis=1)

        return df


class Ratio(Feature):
    """
    Ratio
    """
    prefix = 'ratio'

    def add(self, *args, **kwargs):
        """
        Adds ratio

        @return pd.DataFrame
        """
        df = kwargs.get('df')
        lags_qty = kwargs.get('auto_ml').config.get('lags_qty')
        lags_filter = kwargs.get('auto_ml').config.get('lags_filter')

        for l in filter(
                lambda x: True if lags_filter is None else x in [0] + lags_filter,
                range(lags_qty + 1)):
            df[f'{self.prefix}_{l}'] = df[f'{FACT_COL}_{l}'] / df[
                f'{FACT_COL}_sb_{l}'].replace(0, 1e-4)

        return df

    def recovery(self, *args, **kwargs):
        """
        Recovers values

        @return pd.DataFrame
        """
        df_result = kwargs.get('df_result')
        df = kwargs.get('df')
        method_name = kwargs.get('method_name')
        X_test = kwargs.get('X_test')

        df_result['recovered'] = df.loc[
            df.index.isin(X_test.index),
            f'{FACT_COL}_sb'] * df_result['Y']
        df_result['method'] = method_name

        return df_result


class AbsDelta(Feature):
    """
    Lags absolute deltas
    """
    prefix = 'delta'

    def add(self, *args, **kwargs):
        """
        Adds lags delta

        @return pd.DataFrame
        """
        df = kwargs.get('df')
        lags_qty = kwargs.get('auto_ml').config.get('lags_qty')
        lags_filter = kwargs.get('auto_ml').config.get('lags_filter')

        df = self._add_block(df, FACT_COL, lags_qty, lags_filter, self.prefix)
        df = self._add_block(df, f'{FACT_COL}_sb', lags_qty, lags_filter, f'{self.prefix}_sb')

        return df

    def recovery(self, *args, **kwargs):
        """
        Recovers values

        @return pd.DataFrame
        """

        df_result = kwargs.get('df_result')
        df = kwargs.get('df')
        method_name = kwargs.get('method_name')
        X_test = kwargs.get('X_test')

        df_result['recovered'] = df.loc[
            df.index.isin(X_test.index),
            f'{FACT_COL}_1'] + df_result['Y']
        df_result['method'] = method_name

        return df_result

    def _add_block(self, df, column, lags_qty, lags_filter, prefix):
        """
        Adds a features block

        @return pd.DataFrame
        """
        df[f'{prefix}_0'] = df[f'{column}_0'] - df[f'{column}_1']

        for l in filter(
                lambda x: True if lags_filter is None else x in [0] + lags_filter,
                range(1, lags_qty + 1)):
            df[f'{prefix}_{l}'] = df[f'{column}_{l}'] - df[f'{column}_{l + 1}']

        return df

