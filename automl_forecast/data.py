from datetime import datetime

import urllib
import platform

import pandas as pd

from sqlalchemy import create_engine, exc

from automl_forecast import (
    BACKTEST, FACT_COL, LAYER_FACT, LAYER_FORECAST
)


class Data:
    """
    Data manipulation class
    """
    config = {
        'conn_str': None,
        'db_name': None,
        'base_cols': ['dept_id', 'report_date', 'grp', 'tb', 'gosb', ],
        'groups': (),
        'to_sql_chunksize': 50,
        'to_sql_kwargs': dict(
            schema='py_out',
            if_exists='append',
            index=False
        ),
        'meta': {
            'forecast_id': int(datetime.now().timestamp()),
            'created_dt': datetime.now().date(),
            'created_dttm': datetime.now().today().replace(microsecond=0)
        },
    }

    def __init__(self, config=None):
        """
        Initialization
        """
        # TODO check that db_name is defined
        self.config = self.config.copy()
        if config is not None and isinstance(config, dict):
            for key, value in config.items():
                self.config[key] = value

        if self.config.get('conn_str') is None:
            self.config['conn_str'] = self._get_conn_str()

        self._set_engine()
        self.config['to_sql_kwargs']['con'] = self.engine

    def get_fact(self):
        """
        Returns the dataframe of facts
        
        @return pd.DataFrame
        """
        df_fact = self.config.get('df_fact') #{self.config.get('table_suffix', '')}
        if df_fact is None:
            df_fact = pd.read_sql(
                f"""
                SELECT 
                    * 
                FROM
                    [py_in].[t_automl_fact]
                WHERE
                    [grp] In ({','.join([f"{gr}"for gr in self.config.get('groups') ])})
                ORDER BY 
                    [metric]
                    ,[report_date]
                """, self.engine)

            df_fact['report_date'] = pd.to_datetime(df_fact['report_date'])

            if 'layer' not in df_fact.columns:
                df_fact['layer'] = LAYER_FACT

            df_fact = df_fact[self.config.get('base_cols') +
                [col for col in df_fact.columns if col not in self.config.get('base_cols')]]

            fact_max_date = df_fact['report_date'].max()
            df_corr = self._get_correction()

            if 'layer' not in df_corr.columns:
                df_corr['layer'] = LAYER_FORECAST

            df_corr['fact'] = 0

            df_fact = pd.concat([df_fact, df_corr.loc[df_corr['report_date'] > fact_max_date]])

        return df_fact

    def _get_correction(self):
        """
        Returns the forecast correction

        @return pd.DataFrame
        """
        #{self.config.get('table_suffix', '')
        df = pd.read_sql(
            f"""
            SELECT
                *
            FROM
               [py_in].[t_automl_correction]
            WHERE
                [grp] In ({','.join([f"{gr}"for gr in self.config.get('groups') ])})
            ORDER BY 
                [metric]
                ,[report_date]
        """, self.engine)

        df['report_date'] = pd.to_datetime(df['report_date'])

        return df

    def load_to_db(self, df, area): #table_name):
        """
        Loads a dataframe into a database

        @param df          pd.DataFrame dataframe
        @param area        string       BACKTEST/FORECAST

        @return None
        """
        try:
            df = df.assign(**self.config.get('meta'))
            df.to_sql(
                name=f't_automl{self.config.get("table_suffix", "")}_{area}',
                **self.config.get('to_sql_kwargs'),
                method='multi',
                chunksize=self.config.get('to_sql_chunksize'))
        except exc.IntegrityError:
            # TODO logging
            raise exc.IntegrityError
        
    def get_error(self, mean_func, lag):
        """
        Returns mean of error according to a method and lag

        @param mean_func string function name for mean error calculation
        @param lag       int    lag

        @return pd.DataFrame
        """
        errors = {}
        df = pd.read_sql(
            f"""
                SELECT
                    metric,
                    method,
                    error = CASE WHEN [{FACT_COL}] = 0 THEN 0 ELSE ABS([recovered] / [{FACT_COL}] - 1) END
                FROM
                    [py_out].[t_automl{self.config.get('table_suffix', '')}_backtest]
                WHERE 
                   lag={lag}
            """, self.engine)
        metrics = df['metric'].drop_duplicates()
        methods = df['method'].drop_duplicates()
        for metric in metrics:
            errors[metric] = {
                method: getattr(
                    df.loc[(df['metric'] == metric)
                            & (df['method'] == method), 'error'],
                    mean_func)() for method in methods}
        return errors

    def clear_old_data(self, area=BACKTEST):
        """
        Deletes old data according to a layer

        @param area string BACKTEST/FORECAST

        @return None
        """
        self.engine.execute(
            f"""
                DELETE 
                FROM 
                [py_out].[t_automl{self.config.get('table_suffix', '')}_{area}]
            """)

    def _get_conn_str(self):
        """
        Returns a connection string
        """
        driver = '{ODBC Driver 17 for SQL Server}' \
            if platform.system() == 'Linux' else '{SQL Server}'
        credential = open('/etc/odbc_mssql_creditionals.secret').read() \
            if platform.system() == 'Linux' else 'trusted_connection=yes'

        return 'mssql+pyodbc:///?odbc_connect=' + \
            urllib.parse.quote_plus(
                f'driver={driver};server={self.config.get("server")};{credential}')

    def _set_engine(self):
        """
        Sets the SQLAlchemy engine

        @return None
        """
        self.engine = create_engine(
            self.config.get('conn_str'),
            fast_executemany=True
        )
        self.engine.execute(f"USE [{self.config.get('db_name')}]")
