from sklearn.linear_model import (
    HuberRegressor, Lasso, Lars, LassoLars, Ridge,
    SGDRegressor, ElasticNet, BayesianRidge, TweedieRegressor
)
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR

from sklearn.metrics import median_absolute_error
import xgboost as xgb

from automl_forecast import (
    RANDOM_STATE
)


def objective_hr(trial, X_train, Y_train):
    """
    Optuna objective function for HuberRegressor

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = HuberRegressor(
        alpha=trial.suggest_loguniform('alpha', 1e-4, 0.01),
        epsilon=trial.suggest_loguniform('epsilon', 1.001, 1.35),
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_rf(trial, X_train, Y_train):
    """
    Optuna objective function for RandomForestRegressor

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = RandomForestRegressor(
        random_state=RANDOM_STATE,
        max_depth=int(trial.suggest_loguniform('max_depth', 2, 32)),
        oob_score=True
    )
    regressor.fit(X_train, Y_train)
    return regressor.oob_score_


def objective_en(trial, X_train, Y_train):
    """
    Optuna objective function for ElasticNet

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = ElasticNet(
        random_state=RANDOM_STATE,
        alpha=trial.suggest_loguniform('alpha', 1e-4, 1.0),
        l1_ratio=trial.suggest_loguniform('l1_ratio', 1e-4, 1.0),
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_rr(trial, X_train, Y_train):
    """
    Optuna objective function for Ridge

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = Ridge(
        random_state=RANDOM_STATE,
        alpha=trial.suggest_loguniform('alpha', 1e-4, 1.0),
        solver=trial.suggest_categorical(
            'solver',
            ['auto', 'svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag', 'saga']
        )
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_ls(trial, X_train, Y_train):
    """
    Optuna objective function for Lasso

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = Lasso(
        random_state=RANDOM_STATE,
        alpha=trial.suggest_loguniform('alpha', 1e-4, 1.0),
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_ll(trial, X_train, Y_train):
    """
    Optuna objective function for Lasso

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = LassoLars(
        random_state=RANDOM_STATE,
        alpha=trial.suggest_loguniform('alpha', 1e-4, 1.0),
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_la(trial, X_train, Y_train):
    """
    Optuna objective function for Lars

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = Lars(
        random_state=RANDOM_STATE,
        n_nonzero_coefs=trial.suggest_int('n_nonzero_coefs', 1, 500),
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_br(trial, X_train, Y_train):
    """
    Optuna objective function for BayesianRidge

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = BayesianRidge(
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_sg(trial, X_train, Y_train):
    """
    Optuna objective function for SGDRegressor

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = SGDRegressor(
        random_state=RANDOM_STATE,
        alpha=trial.suggest_loguniform('alpha', 1e-4, 1e-2),
        l1_ratio=trial.suggest_loguniform('l1_ratio', 0.01, 1.0),
        learning_rate=trial.suggest_categorical(
            'learning_rate',
            ['invscaling', 'adaptive', ]),
        loss=trial.suggest_categorical(
            'loss',
            ['squared_loss','huber', ]
        )
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_tr(trial, X_train, Y_train):
    """
    Optuna objective function for TweedieRegressor

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = TweedieRegressor(
        power=trial.suggest_int('power', 0, 4),
        alpha=trial.suggest_loguniform('alpha', 1e-4, 1),
    )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_sr(trial, X_train, Y_train):
    """
    Optuna objective function for SVR

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = SVR(
        C=trial.suggest_loguniform('C', 1e-2, 1),
        kernel='linear',
     )
    regressor.fit(X_train, Y_train)
    return regressor.score(X_train, Y_train)


def objective_xb(trial, X_train, Y_train):
    """
    Optuna objective function for XGBRegressor

    @param trial   object         Optuna Trial object
    @param X_train pd.Dataframe   train set
    @param Y_train pd.Dataframe   target

    @return float
    """
    regressor = xgb.XGBRegressor(
        random_state=RANDOM_STATE,
        reg_alpha=trial.suggest_float("reg_alpha", 1e-8, 1.0, log=True),
        reg_lambda=trial.suggest_float("reg_lambda", 1e-8, 1.0, log=True),
    )
    regressor.fit(X_train, Y_train)
    return median_absolute_error(Y_train, regressor.predict(X_train))
