from functools import partial

import pandas as pd
from pandas.tseries.offsets import MonthEnd

from sklearn.linear_model import HuberRegressor
from sklearn.ensemble import RandomForestRegressor
import xgboost as xgb

import optuna

from automl_forecast import (
    DEFAULT, BACKTEST, FORECAST,
    LAYER_FACT, LAYER_FORECAST, FACT_COL, GOSB_COL, GROUP_COL,
    RANDOM_STATE
)


class AutoML:
    """
    Auto ML class
    """
    config = {
        'methods': {
            'hr': HuberRegressor(max_iter=5000),
            'rf': RandomForestRegressor(random_state=RANDOM_STATE),
            'xb': xgb.XGBRegressor(
                random_state=RANDOM_STATE,
                booster='gblinear')
        },
        'is_calculate_backtest': True,
        'is_calculate_forecast': True,
        'is_load_backtest_to_db': True,
        'is_load_forecast_to_db': True,
        'auto_lag': 6,
        'backtest_months': 6,
        'lags_qty': 12,
        'is_save_data_frames': True
    }
    data_frames = {}
    methods_params = None

    def __init__(self, config=None):
        """
        Initialization
        """
        self.config = self.config.copy()
        if config is not None and isinstance(config, dict):
            for key, value in config.items():
                self.config[key] = value

    def calculate(self):
        """
        The main method of backtesting and forecasting

        @return None
        """
        try:
            if self.config.get('is_calculate_backtest') \
                    or self.config.get('is_calculate_forecast'):
                self._clear_old_data()

                df_data = self.config.get('df') if 'df' in self.config.keys() \
                    else self.config.get('data').get_fact()

                fact_max_date = df_data.loc[df_data['layer'] == LAYER_FACT]['report_date'].max()
                forecast_date = None
                forecast_months = self._get_forecast_months(df_data)

                metrics = self.config.get(
                    'metrics',
                    df_data['metric'].drop_duplicates().to_list()
                )

                for metric in metrics:
                    df_base = df_data.loc[df_data['metric'] == metric]
                    for lag in range(max(
                            self.config.get('backtest_months'),
                            len(forecast_months))):
                        if lag < len(forecast_months):
                            forecast_date = forecast_months[lag]
                        train_test_split_date = fact_max_date + MonthEnd(
                            lag - self.config.get('backtest_months'))

                        groups = df_base.groupby(by=[GROUP_COL])
                        for group in groups.groups.keys():
                            df = groups.get_group(group)
                            if self.config.get('features') is not None:
                                if 'df' not in self.config.keys():
                                    df = self._add_lags(
                                        df,
                                        FACT_COL,
                                        lag,
                                        self.config.get('lags_qty') + 1
                                    )
                                    df.dropna(
                                        subset=[col for col in df.columns if col.startswith(FACT_COL)],
                                        inplace=True)
                                df = self._complete_dataset(df, lag, metric)
                            prefix = self._get_prefix(metric)
                            target_col = f'{prefix}_0'

                            X_cols = [
                                col for col in df.columns
                                if col.startswith(prefix) and col != target_col]

                            self._set_methods_params(
                                df, lag, metric,
                                fact_max_date, X_cols, prefix,
                            )

                            if self.config.get('is_calculate_backtest'):
                                if lag < self.config.get('auto_lag'):
                                    self._calculate_result(
                                        df, lag, metric,
                                        fact_max_date, X_cols, prefix,
                                        train_test_split_date)

                            if self.config.get('is_calculate_forecast'):
                                if lag < len(forecast_months):
                                    self._calculate_result(
                                        df, lag, metric,
                                        fact_max_date, X_cols, prefix,
                                        train_test_split_date,
                                        forecast_date, FORECAST)
        except BaseException as e:
            print(e)

    def get_data_frames(self):
        """
        Returns a dict of data frames with intermediate calculations
        """
        return self.data_frames

    def get_quality(self, mean_func='median', lag=0):
        """
        Returns models quality as a dictionary

        @param mean_func string function name for mean error calculation
        @param lag       int    lag

        @return dict
        """
        return self.config.get('data').get_error(mean_func, lag)

    def _complete_dataset(self, df, lag, metric, area=BACKTEST):
        """
        Completes a dataset with artificial features

        @param df     pd.DataFrame dataframe
        @param lag    int          lag
        @param metric string       metric name

        @return pd.DataFrame
        """

        if isinstance(self.config.get('features'), dict):
            df = self.config.get('features').get(metric).add(
                **AutoML._get_locals(locals())
            ) if self.config.get('features').get(metric) \
                else self.config.get('features').get(DEFAULT).add(
                **AutoML._get_locals(locals())
            )

        df.set_index(
            self.config.get('data').config.get('base_cols'),
            drop=False,
            inplace=True)
        df.sort_index(inplace=True)

        return df

    def _get_prefix(self, metric):
        """
        Returns a features prefix

        @param metric string metric name

        @return string
        """
        if isinstance(self.config.get('features'), dict):
            prefix = self.config.get('features').get(metric).prefix \
                if self.config.get('features').get(metric) \
                else self.config.get('features').get(DEFAULT).prefix
        else:
            prefix = self.config.get('feature_prefix')
        return prefix

    def _get_forecast_months(self, df):
        """
        Returns forecast months quantity

        @return int
        """
        forecast_months = sorted(list(
            set(df.loc[df['layer'] == LAYER_FORECAST]['report_date'].to_list())))

        if self.config.get('forecast_months') is not None:
            forecast_months = forecast_months[:self.config['forecast_months']]
        print (forecast_months)
        return forecast_months

    def _calculate_result(self, df, lag, metric,
                          fact_max_date, X_cols, prefix,
                          train_test_split_date, forecast_date=None, area=BACKTEST):
        """
        Calculates the result according to an area and saves it into a database
        """
        X_train, X_test, Y_train = self._train_test_split(
            df.loc[df['report_date'] <= fact_max_date] if area == BACKTEST else df,
            X_cols,
            f'{prefix}_0',
            train_test_split_date,
            forecast_date,
            fact_max_date,
            area)

        df_result = df.loc[
            df.index.isin(X_test.index),
            self.config.get('data').config.get('base_cols')
        ]

        if area == BACKTEST:
            df_result[FACT_COL] = df.loc[df.index.isin(X_test.index), FACT_COL]
            df_result['lag'] = lag
        df_result['metric'] = metric

        for method_name, method in self.config.get('methods').items():

            df_result = self._add_predicted(df_result, metric, method_name, X_train, Y_train, X_test)

            if self.config.get('is_save_data_frames'):
                self.data_frames[
                    f'{area}_{metric}_{method_name}_{lag}'] = {
                    'X_train': X_train,
                    'Y_train': Y_train,
                    'X_test': X_test,
                    'df_result': df_result
                }

            if isinstance(self.config.get('features'), dict):
                df_result = self.config.get('features').get(metric).recovery(
                    **AutoML._get_locals(locals())
                ) if self.config.get('features').get(metric) \
                    else self.config.get('features').get(DEFAULT).recovery(
                    **AutoML._get_locals(locals())
                )
            else:
                df_result['method'] = method_name
                df_result['recovered'] = df.loc[
                    df.index.isin(X_test.index),
                    f'{FACT_COL}'] * df_result['Y']

            if self.config.get(f'is_load_{area}_to_db'):
                self.config.get('data').load_to_db(df_result, area)

    def _clear_old_data(self):
        """
        Deletes old data

        @return None
        """
        if self.config.get('is_calculate_backtest'):
            if self.config.get('is_load_backtest_to_db'):
                self.config.get('data').clear_old_data(BACKTEST)

        if self.config.get('is_calculate_forecast'):
            if self.config.get('is_load_forecast_to_db'):
                self.config.get('data').clear_old_data(FORECAST)

    def _add_predicted(self, df, metric, method_name, X_train, Y_train, X_test):
        """
        Adds a predicted values to backtest and forecast

        @param df           pd.DataFrame dataframe
        @param metric       string       metric name
        @param method_name  string       method name
        @param X_train      pd.DataFrame train set
        @param Y_train      pd.Series    target
        @param X_test       pd.DataFrame test set

        @return pd.DataFrame
        """

        method = self.config.get('methods')[method_name]
        if self.methods_params.get(metric) is not None:
            if self.methods_params.get(metric).get(method_name) is not None:
                for key, param in self.methods_params[metric][method_name].items():
                    setattr(method, key, param)
        method.fit(X_train, Y_train)
        if len(X_test):
            df['Y'] = method.predict(X_test)

        return df

    def _set_methods_params(self, df, lag, metric,
                          fact_max_date, X_cols, prefix):
        """

        """
        if self.methods_params is None:

            self.methods_params = {}

            X_train, _, Y_train = self._train_test_split(
                df,
                X_cols,
                f'{prefix}_0',
                fact_max_date - MonthEnd(self.config.get('backtest_months')),
            )

            for method_name, method in self.config.get('methods').items():
                if self.config.get('objective') is not None:
                    if method_name in self.config.get('objective').keys():
                        study = optuna.create_study()
                        objective = partial(
                            self.config.get('objective').get(method_name),
                            X_train=X_train,
                            Y_train=Y_train)

                        study.optimize(
                            objective,
                            n_trials=self.config.get('objective_n_trials', 100))
                        if metric not in self.methods_params.keys():
                            self.methods_params[metric] = {}
                        self.methods_params[metric][method_name] = study.best_params
            print(self.methods_params)

    def _add_lags(self, df, column, lag_shift, lags_qty):
        """
        Adds lags to facts dataframe

        @param df          pd.DataFrame dataframe
        @param column      string       columns name
        @param lag_shift   int          laf shift
        @param lags_qty    int          lags quantity

        @return pd.DataFrame
        """
        def add_lag(df, column, lag_shift):
            df.loc[:, f'{column}_0'] = df[column]

            for lag in range(1, lags_qty + 1):
                df.loc[:, f'{column}_{lag}'] = df[column].shift(lag + lag_shift)

            return df

        gosbs = df[self.config.get('gosb_column', GOSB_COL)].drop_duplicates()

        df_lags = None

        for gosb in gosbs:
            df_gosb = df.loc[df[self.config.get('gosb_column', GOSB_COL)] == gosb]

            df_gosb = add_lag(df_gosb, column, lag_shift)
            df_gosb = add_lag(df_gosb, f'{column}_sb', lag_shift)

            if df_lags is None:
                df_lags = pd.DataFrame([], columns=df_gosb.columns)

            df_lags = pd.concat([df_lags, df_gosb])

        return df_lags

    def _train_test_split(self, df, X_cols, target_col, train_test_split_date,
                          forecast_date=None, fact_max_date=None, area=BACKTEST):
        """
        Returns a tuple: (train set, target, test set) for time series

        @param df                     pd.DataFrame dataframe
        @param X_cols                 list         columns list of a train set
        @param target_col             str          target column name
        @param train_test_split_date  date         split date
        @param forecast_date          date         forecast date
        @param fact_max_date          date         last fact date
        @param area                   str          BACKTEST/FORECAST

        @return tuple
        """
        if area == BACKTEST:
            return (df.loc[df['report_date'] <= train_test_split_date, X_cols],
                    df.loc[df['report_date'] > train_test_split_date, X_cols],
                    df.loc[df['report_date'] <= train_test_split_date, target_col])
        elif area == FORECAST:
            return (df.loc[df['report_date'] <= fact_max_date, X_cols],
                    df.loc[df['report_date'] == forecast_date, X_cols],
                    df.loc[df['report_date'] <= fact_max_date, target_col])

    @staticmethod
    def _get_locals(locals):
        """
        Returns locals

        @param locals dict dictionary of local variables

        @return dict
        """
        locals['auto_ml'] = locals['self']
        del locals['self']
        return locals
