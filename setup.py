#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'Sphinx==1.8.5',
    'pandas==1.0.5',
    'numpy==1.19.0',
    'python-dateutil==2.8.1',
    'sqlalchemy==1.3.18',
    'joblib==0.16.0',
    'scikit-learn==0.23.1',
    'scipy==1.5.1',
    'sklearn==0.0',
    'threadpoolctl==2.1.0',
    'pyodbc==4.0.30',
    'xgboost==1.1.1',
    'xlrd==1.2.0',
    'optuna==2.0.0',
]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest>=3', ]

setup(
    author="Eugena Mikhaylikova",
    author_email='eugena.mihailikova@gmail.com',
    python_requires='>=3.5',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Python package for automatic forecasting based on Machine Learning methods",
    # entry_points={
    #     'console_scripts': [
    #         'automl_forecast=automl_forecast.cli:main',
    #     ],
    # },
    install_requires=requirements,
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='automl_forecast',
    name='automl_forecast',
    packages=find_packages(include=['automl_forecast', 'automl_forecast.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://stash.delta.sbrf.ru/users/mihaylikova-ea/repos/automl_forecast/',
    version='0.1.0',
    zip_safe=False,
)
