Модуль AutoML
=============
Инструмент для разработчика моделей, который позволяет по подготовленным в нужном формате 
фактам вычислить бэктест и прогноз и записать в БД. Факты дополнительно могут быть разбиты 
по ГОСБам, клиентским группам и метрикам, в этом случае модели для прогнозирования будут строиться внутри группы.

Модуль может работать в двух режимах:
- полный цикл вычислений
- частичный цикл вычислений

**Полный цикл** включает запрос подготовленных фактов из БД, автоматическое вычисление признаков, перебор методов машинного обучения, вычисление бэктеста и прогноза, запись результатов в БД и вывод на экран наилучшего метода по данным бэктеста.

**Частичный цикл** включает считывание датасета с подготовленными заранее признаками с диска, перебор методов машинного обучения, вычисление бэктеста и прогноза, запись результатов в БД и вывод на экран наилучшего метода по данным бэктеста. Частичный цикл сделан для случаев, когда вычисление признаков внутри модуля слишком сложно или нецелесообразно (исключительные случаи). Мы стремимся к тому, чтобы частичный цикл никогда не использовался.

Инструмент позволяет разработчику дорабатывать существующие и создавать новые способы вычисления признаков и новые функции для подбора гиперпараметров. Впоследствии эти наработки могут быть включены в основной модуль для использования другими разработчиками.

В то же время инструмент может использоваться для создания продукта прогнозирования без участия разработчков, но в этом случае вероятны неоптимальные результаты, поэтому, участие разработчика для настройки модели все же предполагается.

Основные возможности
--------------------
- Выбор способа прогнозирования (полный, частичный цикл)
- Выбор методов для генерации признаков с точностью до метрики
- Использование любых методов машинного обучения
- Автонастройка гиперпараметров для выбранных методов машинного обучения
- Просмотр промежуточных вычислений
- Включение и отключение вычисления бэктеста и прогноза
- Включение и отключение записи бэктеста и прогноза в базу данных
- Запись результатов в таблицы с указанным суффиксом в случае необходимости, для 
сохранения результатов в основных

Требования
----------
База данных модели MS SQL.

Версия Python: 3.7

Стуктура данных
---------------
Для каждой модели должна быть создана своя база данных.

Таблицы, необходимые для работы модуля:

- **[py_in].[t_automl_fact]** - таблица фактов

- **[py_in].[t_automl_correction]** - таблица корректировки (содержит суммарный прогноз для восстановления прогнозных значений)
    
- **[py_out].[t_automl_backtest]** - таблица с вычисленным бэктестом

- **[py_out].[t_automl_forecast]** - таблица с вычисленным прогнозом


Установка, проверка и обновление
--------------------------------
**Установка**

Создание виртуального пространства:

```shell
virtualenv .automl

source .automl/bin/activate
```

Затем:
```shell
pip install -e git+https://eugena@bitbucket.org/eugena/automl_forecast.git#egg=automl_forecast
```
либо:
```shell
git clone https://eugena@bitbucket.org/eugena/automl_forecast.git

cd automl_forecast

python setup.py install
```

**Проверка работы**
```shell
ipython
```
затем:
```python
import automl_forecast
```
импорт должен пройти успешно.

**Установка новой версии**
```shell
source .automl/bin/activate

pip install -U -e git+https://eugena@bitbucket.org/eugena/automl_forecast.git#egg=automl_forecast
```

**Добавление виртуального пространства AutoML** 

Добавление виртуального пространства AutoML для использования на JupyterHub
```shell
source .automl/bin/activate

pip install ipykernel

python -m ipykernel install --user --name .automl --display-name 'AutoML'
```

Быстрый старт
-------------
1.	Создать базу данных модели
2.	Создать таблицы:

Таблица фактов:

```sql
CREATE TABLE [py_in].[t_automl_fact](
    [report_date] [date] NOT NULL,
    [tb] [int] NULL,
    [gosb] [int] NULL,
    [dept_id] [int] NULL,
    [grp] [bigint] NOT NULL,
    [metric] [varchar](100) NOT NULL,
    [fact] [float] NOT NULL,
    [fact_sb] [float] NULL
) ON [PRIMARY]
GO
```

Таблица корректировки:
```sql
CREATE TABLE [py_in].[t_automl_correction](
    [report_date] [date] NOT NULL,
    [tb] [int] NULL,
    [gosb] [int] NULL,
    [dept_id] [int] NULL,
    [grp] [bigint] NOT NULL,
    [metric] [varchar](100) NOT NULL,
    [fact] [float] NULL,
    [fact_sb] [float] NOT NULL
) ON [PRIMARY]
GO
```

Таблица бэктеста:
```sql
CREATE TABLE [py_out].[t_automl_backtest](
    [report_date] [date] NOT NULL,
    [tb] [int] NULL,
    [gosb] [int] NULL,
    [dept_id] [int] NULL,
    [grp] [bigint] NOT NULL,
    [metric] [varchar](100) NOT NULL,
    [lag] [tinyint] NOT NULL,
    [fact] [float] NULL,
    [method] [char](2) NOT NULL,
    [Y] [float] NOT NULL,
    [recovered] [float] NOT NULL,
    [forecast_id] [bigint] NOT NULL,
    [created_dt] [date] NOT NULL,
    [created_dttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-20200810-092356] ON [py_out].[t_automl_backtest]
(
    [report_date] ASC,
    [tb] ASC,
    [gosb] ASC,
    [dept_id] ASC,
    [grp] ASC,
    [lag] ASC,
    [fact] ASC,
    [method] ASC,
    [metric] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
```

Таблица прогноза:
```sql
CREATE TABLE [py_out].[t_automl_forecast](
    [report_date] [date] NOT NULL,
    [tb] [int] NULL,
    [gosb] [int] NULL,
    [dept_id] [int] NULL,
    [grp] [bigint] NOT NULL,
    [metric] [varchar](100) NOT NULL,
    [method] [char](2) NOT NULL,
    [Y] [float] NOT NULL,
    [recovered] [float] NOT NULL,
    [forecast_id] [bigint] NOT NULL,
    [created_dt] [date] NOT NULL,
    [created_dttm] [datetime] NOT NULL
) ON [PRIMARY]
GO

CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-20200810-092507] ON [py_out].[t_automl_forecast]
(
    [report_date] ASC,
    [tb] ASC,
    [gosb] ASC,
    [dept_id] ASC,
    [grp] ASC,
    [method] ASC,
    [metric] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
```
3.	Залить факты и корректировку в таблицы **[py_in].[t_automl_fact]** и **[py_in].[t_automl_correction]**. 
4.  Сконфигурировать модуль и запустить расчет

Пример вызова:
```python
from sklearn.linear_model import (
    HuberRegressor, LinearRegression, LassoLars, 
    SGDRegressor, ElasticNet, BayesianRidge
)

import xgboost as xgb

from automl_forecast import (
    DEFAULT, RANDOM_STATE
)
from automl_forecast.automl_forecast import AutoML
from automl_forecast.data import Data
from automl_forecast.features import RelGrowth, Ratio, SimpleLags, AbsDelta

from automl_forecast.objectives import (
    objective_hr, objective_xb
)

try:
    automl = AutoML(
        config={
            'methods': {
                'lr': LinearRegression(),
                'hr': HuberRegressor(max_iter=5000),
                'en': ElasticNet(random_state=RANDOM_STATE),
                'xb': xgb.XGBRegressor(
                    random_state=RANDOM_STATE,
                    booster='gblinear'
                ),
                'sg': SGDRegressor(
                    random_state=RANDOM_STATE,
                    loss='huber',
                ),
                'll': LassoLars(random_state=RANDOM_STATE),
                'br': BayesianRidge(),
            },
            'objective': {
                'hr': objective_hr,
                'xb': objective_xb,
            },
            'objective_n_trials': 100,
            'data': Data(
                config={
                    'server': '[HOST]',
                    'db_name': 'Model_Passiv_FL_ML',
                    'base_cols': ['dept_id', 'report_date', 'grp', 'tb', 'gosb', ],
                    'groups': (1e+007, 5e+008,),
                    'to_sql_chunksize': 50,
                }),
            'features': {
                'clients_count': RelGrowth(),
                DEFAULT:  Ratio(),
            },
            'auto_lag': 2,
            'backtest_months': 2,
            'forecast_months': 2,
            'lags_qty': 12,
            'metrics': ['clients_count', ],
            'is_calculate_backtest': True,
            'is_calculate_forecast': True,
            'is_load_backtest_to_db': True,
            'is_load_forecast_to_db': True,
        }
    )
    automl.calculate()

    quality = automl.get_quality()
    for metric in quality.keys():
        print(f'{metric}:')
        print({k: v for k, v in sorted(quality[metric].items(), key=lambda x: x[1])})
except BaseException as e:
    print(e)
```
Результат:
 
```python
# Результаты подбора гиперпараметров
{'clients_count': {'hr': {'alpha': 0.00086349056236007, 'epsilon': 1.0539934842866163}, 'xb': {'reg_alpha': 9.206118974463766e-06, 'reg_lambda': 0.00012265488210415538}}}

# Ошибка на бэктесте по метрикам, сортировка методов по убыванию ошибки
clients_count:
{'hr': 0.005130931796415439, 'lr': 0.005704129358194254, 'br': 0.0061392769736787756, 'en': 0.007503763236799077, 'll': 0.007503763236799077, 'xb': 0.020049264906429443, 'sg': 0.06917385822368105}
```
5.	Бэктест и прогноз будут загружены в таблицы **[py_out].[t_automl_backtest]** и **[py_out].[t_automl_forecast]**


Больше примеров вызова
-------------
[Тесты модуля](tests/test_automl_forecast.py)


Описание файлов модуля
----------------------
**Жирным** выделены файлы, которые могут дорабатываться разработчиками моделей
- [automl_forecast.py](automl_forecast/automl_forecast.py) - объект для работы с ML частью
- [data.py](automl_forecast/data.py) - объект для работы с базой данных
- **[features.py](automl_forecast/features.py)** - объекты для генерации признаков
- **[objectives.py](automl_forecast/objectives.py)** - набор функций подбора гиперпараметров


Описание конфигуратора
----------------------

**data** - object типа Data, *обязательный*,
конфигуратор объекта для работы с базой данных

пример:
```python
'data': Data(
    config={
        'conn_str': 'mssql+pyodbc://MSSQL_DSN',
        'db_name': 'Model_Passiv_FL_ML',
        'table_suffix': '_acquiring',
        'base_cols': ['gosb', 'report_date', 'tb', 'grp', ],
    }
)
```

**methods** - dict, *необязательный*,
методы машинного обучения, которые будут использоваться модулем для расчетов

пример:
```python
'methods': {
    'lr': LinearRegression(),
    'hr': HuberRegressor(max_iter=5000),
    'en': ElasticNet(random_state=RANDOM_STATE),
    'rr': Ridge(random_state=RANDOM_STATE),
    'rf': RandomForestRegressor(random_state=RANDOM_STATE),
}
```

**objective** - dict, *необязательный*,
список функций для подбора гиперпараметров

пример:
```python
'objective': {
    'hr': objective_hr,
    'en': objective_en,
    'rf': objective_rf,
}
```

**objective_n_trials**, int, *необязательный*,
число итераций для подбора гиперпараметров


**df**, pd.DataFrame, *необязательный*,
файл с данными для частичного цикла вычислений, если задан, то факты и корректировка из базы данных браться не будут и не будет вычисления признаков. Предполагается, что признаки уже вычислены вне модуля.


**gosb_column**, str, *необязательный*,
колонка, для группировки по ГОСБам
по умолчанию: _'dept_id'_


**feature_prefix**, str, *необязательный*,
префикс признаков, используется только при частичном цикле вычислений.


**is_calculate_backtest**, bool, *необязательный*, по умолчанию _True_,
нужно ли вычислять бэктест


**is_load_backtest_to_db**, bool, *необязательный*, по умолчанию _True_,
нужно ли записывать бэктест в базу данных


**is_calculate_forecast**, bool, *необязательный*, по умолчанию _True_,
нужно ли вычислять прогноз


**is_load_forecast_to_db**, bool, *необязательный*, по умолчанию _True_,
нужно ли записывать прогноз в базу данных


**features**, dict, *необязательный*,
правила вычисления фичей для каждой метрики, если метрика не указана, то будет использоваться правило с ключом DEFAULT.
Используется только в полном цикле вычислений.

пример:
```python
'features': {
    'clients_count': RelGrowth(),
    'sdo_by_client_cur_acc': SimpleLags(),
    'balance_by_client_rur_crd': AbsDelta(),
    DEFAULT:  Ratio(),
}
```


**lags_qty**, int, *необязательный*, по умолчанию _12_,
число лагов для вычисления признаков


**lags_filter**, list, *необязательный*, 
фильтр признаков после генерации, должен содержать номера лагов, которые останутся в обучающей и тестовой выборках


**is_features_depend_on_skip_lag**, bool, *необязательный*, по умолчанию _False_,
зависит ли расчет признаков от skip_lag хотя бы для одной метрики. Используется только в полном цикле вычислений.


**auto_lag**, int, *необязательный*, по умолчанию _6_,
число лагов для вычисления бэктеста.


**backtest_months**, int, *необязательный*, по умолчанию _6_,
число месяцев бэктеста.


**forecast_months**, int, *необязательный*, по умолчанию будут использоваться все месяцы таблицы корректировки,
число месяцев прогноза.


**metrics**, list, *необязательный*, по умолчанию будут использоваться все метрики таблицы фактов,
список метрик для вычислений.

**is_save_data_frames**, bool, *необязательный*, по умолчанию _True_,
сохранять ли промежуточные вычисления для последующего анализа

Генераторы признаков
------------------------------

**SimpleLags** - просто лаги

**RelGrowth** - относительный прирост

**Ratio** - отношение к суммарному значению

**AbsDelta** - абсолютный прирост


TODO
-----
1. Архивные таблицы для наблюдения за результатами
2. Автоматический выбор лучшего метода
3. Создание "упаковки" для расчетов без участия разработчика моделей
4. Протестировать модуль на готовность прогнозировать ТБ
5. Verbose режим модуля
6. Запись в базу метода генерации фич

